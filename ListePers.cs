﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intro {
    class ListePers : IEnumerable<Personne> {
        private List<Personne> liste = new List<Personne>();

        public ListePers() {
            Personne p1 = new Personne("paul", "dupond", Genre.H);
            Personne p2 = new Personne("isabelle", "durand", Genre.F);
            Personne p3 = new Personne("Claude", "van Lear", Genre.ND);
            liste.AddRange(new Personne[] { p1, p2, p3 });
        }

        public int NbElem => liste.Count;

        public void AddElem(Personne p) {
            liste.Add(p);
        }

        public bool RemoveElem(Personne p) {
            return liste.Remove(p);
        }

        public Personne this[int n] => liste[n];

        public void SortBy(Comparison<Personne> comp) {
            liste.Sort(comp);
        }

        public IEnumerator<Personne> GetEnumerator() {
            return liste.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return liste.GetEnumerator();
        }
    }
}

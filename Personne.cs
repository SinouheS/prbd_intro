﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intro {
    enum Genre { H, F, ND };
    class Personne {
        public Personne(string nom, string prenom, Genre genre) {
            Nom = nom;
            Prenom = prenom;
            Genre = genre;
        }

        private string nom;
        public string Nom { get => nom; set => nom = value; }

        private string prenom;


        public string Prenom { get => prenom; set => prenom = value; }

        public Genre Genre { get; set; }

        public override String ToString() {
            return $"{prenom} \t {nom} \t {Genre}";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Intro {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// Test Comment
    public partial class MainWindow : Window {

        private ListePers listePers = new ListePers();

        private bool modeAjout = false;
        private bool asc = true;

        public MainWindow() {
            InitializeComponent();
            ConfigComponent();
        }

        private void Clear() {
            listBox.Items.Clear();
            txtBoxNom.Clear();
            txtBoxPrenom.Clear();
        }

        private void ConfigComponent() {
            MakeList();
            ConfigTri();
        }

        private void ConfigTri() {
            comboTri.SelectedIndex = 0;
            rbAsc.IsChecked = true;
            triListe();
        }

        private void MakeList() {
            Clear();
            foreach (var p in listePers)
                listBox.Items.Add(p);
            listBox.SelectedIndex = 0;
        }

        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var p = (Personne)listBox.SelectedItem;
            if (p == null) return;
            txtBoxNom.Text = p.Nom;
            txtBoxPrenom.Text = p.Prenom;
            switch (p.Genre) {
                case Genre.H:
                    rbH.IsChecked = true;
                    break;
                case Genre.F:
                    rbF.IsChecked = true;
                    break;
                case Genre.ND:
                    rbND.IsChecked = true;
                    break;
            }
        }

        private void btcNew_Click(object sender, RoutedEventArgs e) {
            txtBoxPrenom.Clear();
            txtBoxNom.Clear();
            txtBoxNom.Focus();
            rbND.IsChecked = true;
            modeAjout = true;
        }

        private void btcValider_Click(object sender, RoutedEventArgs e) {
            if(modeAjout) {
                listePers.AddElem(
                    new Personne(txtBoxPrenom.Text, txtBoxNom.Text, ChoixGenre())
                );
                modeAjout = false;
            } else {
                var p = (Personne)listBox.SelectedItem;
                if (p == null) return;
                p.Nom = txtBoxNom.Text;
                p.Prenom = txtBoxPrenom.Text;
                p.Genre = ChoixGenre();
            }
            MakeList();
        }

        private Genre ChoixGenre() {
            if (rbH.IsChecked.Value)
                return Genre.H;
            else if (rbF.IsChecked.Value)
                return Genre.F;
            else
                return Genre.ND;
        }

        private void btcSuppr_Click(object sender, RoutedEventArgs e) {
            var p = (Personne)listBox.SelectedItem;
            if (p == null) return;
            listePers.RemoveElem(p);
            MakeList();
        }

        private void comboTri_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            triListe();
        }

        private void triListe() {
            var choixTri = (ComboBoxItem)comboTri.SelectedValue;
            if (choixTri == null) return;
            switch ((String)choixTri.Content) {
                case "Nom":
                    listePers.SortBy((p1, p2) => Compare(p1.Prenom, p2.Prenom, ChoixTri()));
                    break;
                case "Prénom":
                    listePers.SortBy((p1, p2) => Compare(p1.Nom, p2.Nom, ChoixTri()));
                    break;
                case "Genre":
                    listePers.SortBy((p1, p2) => Compare(p1.Genre.ToString(), p2.Genre.ToString(), ChoixTri()));
                    break;
                default:
                    break;
            }
            MakeList();
        }

        private bool ChoixTri() {
            return rbAsc.IsChecked.Value;
        }

        private int Compare(IComparable o1, IComparable o2, bool asc) {
            if (o1 == null && o2 == null)
                return 0;
            else if (o1 == null)
                return asc ? -1 : 1;
            else if (o2 == null)
                return asc ? 1 : -1;
            else
                return asc ? o1.CompareTo(o2) : o2.CompareTo(o1);
        }

        private void rbTri_Checked(object sender, RoutedEventArgs e) {
            triListe();
        }
    }
}
